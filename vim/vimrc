"
" dotphiles : https://github.com/dotphiles/dotphiles
"
" Setup vim and load required plugins before dotvim
"
" Authors:
"   Ben O'Hara <bohara@gmail.com>
"

" Call dotvim
source ~/.vim/dotvim.vim

if has("user_commands")
  set nocompatible
  filetype off
  set rtp+=~/.vim/bundle/vundle/
  call vundle#rc()
  "let g:vundles=['general', 'git', 'hg', 'programming', 'completion', 'ruby', 'python', 'misc']
  "let g:vundles=['general', 'programming']
  let g:vundles=['general', 'programming', 'php', 'ruby', 'git', 'javascript', 'completion', 'html', 'misc']
  "let g:vundles=[]
  let g:neocomplcache_enable_at_startup = 1
  source ~/.vim/vundles.vim
  " ========= Add extra bundles here =============

  " Colors
  " Plugin 'chriskempson/base16-vim'           " Prerequisite for dotvim
  " Plugin 'flazz/vim-colorschemes'            " Color schemes library
  " Plugin 'altercation/vim-colors-solarized'  " Solarized theme

  " Layout
  Plugin 'chrisbra/csv.vim'

  " Syntax Highlighting
  Plugin 'tpope/vim-haml'           " HAML syntax highlighting
  Plugin 'tpope/vim-git'            " Use vim for git-related editing tasks
  Plugin 'othree/html5.vim'         " HTML5 tag support
  Plugin 'kchmck/vim-coffee-script' " CoffeeScript syntax highlighting, indentation, and compiling

  " Syntax enforcers
  Plugin 'tpope/vim-sleuth'    " Automatically adjust indentation standards from file contents
  Plugin 'tpope/vim-endwise'   " if-end autocompletion

  " Command wrappers
  Plugin 'rking/ag.vim'        " Silver Searcher wrapper

  " Ruby
  Plugin 'vim-ruby/vim-ruby'  " Syntax sugar for ruby

  " Filesystem
  Plugin 'tpope/vim-vinegar'  " Better netrw filesystem navigation

  " Other
  Plugin 'tpope/vim-surround'      " Change surrounding quotes, xml tags, etc. with key combo
  Plugin 'Lokaltog/vim-easymotion' " Movement without :<number>direction

  " Powerline
  python from powerline.vim import setup as powerline_setup
  python powerline_setup()
  python del powerline_setup
endif
" Customize to your needs...

filetype plugin indent on

" Indent and EOL characters
set list
set listchars=tab:▸.,eol:¬,trail:☠,
" set listchars=tab:┆·,eol:¬,trail:☠
" set listchars=tab:»·,eol:☠

" color of invisible characters (indent, eol) should be barely visible
highlight NonText ctermfg=Black
highlight SpecialKey ctermfg=Black

" Try to keep lines concise and to-the-point
set colorcolumn=85

" More convenient than '\'
let mapleader = ","

" CtrlP settings
let g:ctrl_p_working_path_mode = 'ra' " prefer '.git' directories as root of project
set wildignore+=*/tmp/*,*.so,*.swp,*.zip  " Ignore temporary files and directories
let g:ctrlp_user_command = 'find %s -type f' " Use native file finding command

" MACROS:

" Un-highlight search terms
nnoremap <leader><space> :noh<cr>
" Find ctags with CtrlP plugin
nnoremap <leader>. :CtrlPTag<cr>
" CtrlP plugin hotkey
" Find within buffer
nnoremap <leader>b :CtrlPBuffer<cr>
" NerdTree open
nnoremap <leader>\| :NERDTree<cr>

" Remove trailing spaces just before writing buffer to file
"autocmd FileType javascript,c,cpp,java,php,ruby autocmd BufWritePre <buffer> :%s/\s\+$//e
" Strip all trailing whitespace from the current file
nnoremap <leader>W :%s/\s+$//<CR>:let @/=''<CR>

set nocursorline


